import 'package:empresas_flutter/app/core/custom_headers/domain/get_custom_headers_usecase.dart';
import 'package:empresas_flutter/app/core/local_data/i_local_data_manager.dart';
import 'package:empresas_flutter/app/core/local_data/local_data_manager.dart';
import 'package:empresas_flutter/app/features/enterprises/data/datasources/enterprises_datasource.dart';
import 'package:empresas_flutter/app/features/enterprises/data/repositories/enterprises_repository_impl.dart';
import 'package:empresas_flutter/app/features/enterprises/domain/repositories/i_enterprises_repository.dart';
import 'package:empresas_flutter/app/features/enterprises/domain/usecases/get_enterprises_usecase.dart';
import 'package:empresas_flutter/app/features/enterprises/presentation/cubit/enterprises_cubit.dart';
import 'package:empresas_flutter/app/features/login/data/datasources/login_datasource.dart';
import 'package:empresas_flutter/app/features/login/data/repositories/login_repository_impl.dart';
import 'package:empresas_flutter/app/features/login/domain/repositories/i_login_repository.dart';
import 'package:empresas_flutter/app/features/login/domain/usecases/login_usecase.dart';
import 'package:empresas_flutter/app/features/login/domain/usecases/save_custom_headers_usecase.dart';
import 'package:empresas_flutter/app/features/login/presentation/cubit/login_cubit.dart';
import 'package:get_it/get_it.dart';

final sl = GetIt.I;

Future<void> init() async {
  sl.registerFactory(
    () => LoginCubit(loginUseCase: sl(), saveCustomHeadersUseCase: sl()),
  );

  sl.registerFactory(
    () => LoginUseCase(loginRepository: sl()),
  );

  sl.registerFactory(
    () => SaveCustomHeadersUseCase(localDataManager: sl()),
  );

  sl.registerFactory(
    () => GetCustomHeadersUsecase(localDataManager: sl()),
  );

  sl.registerFactory<ILoginRepository>(
    () => LoginRepositoryImpl(loginDataSource: sl()),
  );

  sl.registerFactory(() => LoginDataSource());

  sl.registerFactory(
    () => EnterprisesCubit(getEnterprisesUseCase: sl()),
  );

  sl.registerFactory(
    () => GetEnterprisesUseCase(enterprisesRepository: sl()),
  );

  sl.registerFactory<IEnterprisesRepository>(
    () => EnterprisesRepositoryImpl(enterprisesDataSource: sl()),
  );

  sl.registerFactory(
    () => EnterprisesDataSource(),
  );

  sl.registerLazySingleton<ILocalDataManager>(() => LocalDataManager());
}
