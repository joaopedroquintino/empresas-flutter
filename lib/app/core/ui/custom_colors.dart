import 'package:flutter/painting.dart';

abstract class CustomColors {
  static Color backgroundGrey = Color(0xFFF5F5F5);
  static Color mainColor = Color(0xFFE01E69);

  static Color color1GradientLogin = Color(0xFFAA2995);
  static Color color2GradientLogin = Color(0xFF9A8DAA);

  static Color color1GradientHome = Color(0xFF8327BA);
  static Color color2GradientHome = Color(0xFFAF1A7D);

  static Color homeAppBarColor = Color(0xFFB01A88);
}
