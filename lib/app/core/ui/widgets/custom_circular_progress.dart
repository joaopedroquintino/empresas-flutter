import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomCircularProgress extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Transform.rotate(
      angle: pi / 2,
      child: Container(
        child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              height: 40,
              width: 40,
              child: CustomIndicator(clockwise: false),
            ),
            Container(
              height: 20,
              width: 20,
              child: CustomIndicator(clockwise: true),
            ),
          ],
        ),
      ),
    );
  }
}

class CustomIndicator extends StatefulWidget {
  const CustomIndicator({this.clockwise = true}) : super();

  final bool clockwise;
  @override
  _CustomIndicatorState createState() => _CustomIndicatorState();
}

class _CustomIndicatorState extends State<CustomIndicator>
    with TickerProviderStateMixin {
  AnimationController controller;

  @override
  void initState() {
    controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 1),
    )..addListener(() {
        setState(() {});
      });
    controller.repeat(reverse: true);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Transform.rotate(
      angle: widget.clockwise ? pi : 0,
      child: CircularProgressIndicator(
        value: controller.value,
        strokeWidth: 1,
      ),
    );
  }
}
