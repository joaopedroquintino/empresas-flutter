import 'package:empresas_flutter/app/core/ui/custom_colors.dart';
import 'package:flutter/material.dart';

abstract class CustomGradients {
  static Gradient get loginGradient {
    return LinearGradient(
      begin: Alignment.topRight,
      end: Alignment.bottomLeft,
      colors: [
        CustomColors.color1GradientLogin,
        CustomColors.color2GradientLogin,
      ],
    );
  }

  static Gradient get homeGradient {
    return LinearGradient(
      begin: Alignment.topRight,
      end: Alignment.bottomLeft,
      colors: [
        CustomColors.color1GradientHome,
        CustomColors.color2GradientHome,
      ],
    );
  }
}
