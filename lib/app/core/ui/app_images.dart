abstract class AppImages {
  static _image(String name) {
    return 'assets/images/$name.png';
  }

  static String get ioasysLogo => _image('logo');
  static String get ioasysWideLogo => _image('logo_ioasys');
}
