import 'package:empresas_flutter/app/core/ui/fonts.dart';
import 'package:flutter/material.dart';

abstract class Styles {
  static TextStyle get errorStyle {
    return TextStyle(
      fontFamily: Fonts.rubik,
      color: Colors.redAccent,
      fontSize: 12,
      height: 1.3,
    );
  }

  static TextStyle get textFieldStyle {
    return TextStyle(
      fontFamily: Fonts.rubik,
      fontWeight: FontWeight.w300,
      fontSize: 16,
      height: 1.5,
    );
  }

  static TextStyle get signInButtonStyle {
    return TextStyle(
      fontFamily: Fonts.rubik,
      fontWeight: FontWeight.w500,
      fontSize: 16,
      height: 1.3,
    );
  }

  static TextStyle get headingSignInStyle {
    return TextStyle(
      fontFamily: Fonts.rubik,
      fontWeight: FontWeight.w400,
      fontSize: 20,
      height: 1.15,
      color: Colors.white,
    );
  }

  static TextStyle get headingEnterpriseStyle {
    return TextStyle(
      fontFamily: Fonts.rubik,
      fontWeight: FontWeight.w700,
      fontSize: 18,
      height: 1.33,
      color: Colors.white,
    );
  }

  static TextStyle get bodyText {
    return TextStyle(
      fontFamily: Fonts.rubik,
      fontWeight: FontWeight.w300,
      fontSize: 18,
      height: 1.33,
      color: Colors.black,
    );
  }

  static TextStyle get headingText {
    return TextStyle(
      fontFamily: Fonts.rubik,
      fontWeight: FontWeight.w500,
      fontSize: 22,
      height: 1.4,
      color: Colors.black,
    );
  }
}
