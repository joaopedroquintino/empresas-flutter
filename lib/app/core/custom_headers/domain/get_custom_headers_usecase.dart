import 'package:empresas_flutter/app/core/local_data/i_local_data_manager.dart';
import 'package:empresas_flutter/app/features/login/domain/entities/custom_headers_entity.dart';
import 'package:meta/meta.dart';

class GetCustomHeadersUsecase {
  GetCustomHeadersUsecase({
    @required ILocalDataManager localDataManager,
  }) : _localStorage = localDataManager;

  final ILocalDataManager _localStorage;

  Future<CustomHeadersEntity> call() async {
    return _localStorage.getCustomHeaders();
  }
}
