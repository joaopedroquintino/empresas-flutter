import 'package:flutter/material.dart';

abstract class AppNavigator {
  GlobalKey<NavigatorState> get navigationKey;
}

class AppNavigatorProvider {
  AppNavigatorProvider._();

  static final AppNavigatorProvider instance = AppNavigatorProvider._();
  final Map<Type, AppNavigator> _navigatorKeys = {};

  T get<T extends AppNavigator>() {
    return _navigatorKeys[T];
  }

  void add<T extends AppNavigator>(
    T navigator,
  ) {
    if (_navigatorKeys.containsKey(T)) {
      _navigatorKeys[T].navigationKey.currentState.dispose();
    }
    _navigatorKeys[T] = navigator;
  }

  T remove<T extends AppNavigator>() {
    return _navigatorKeys.remove(T);
  }
}
