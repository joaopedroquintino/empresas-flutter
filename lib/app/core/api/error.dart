import 'package:empresas_flutter/app/core/network/response/api_result.dart';
import 'package:empresas_flutter/app/core/network/response/states/apiError/api_error.dart';
import 'package:empresas_flutter/app/core/network/response/states/internal_error.dart';
import 'package:empresas_flutter/app/core/network/response/states/no_internet.dart';
import 'package:empresas_flutter/app/core/utils/strings.dart';
import 'package:equatable/equatable.dart';

class Error extends Equatable {
  Error({
    this.message,
    this.statusCode,
  });

  final int statusCode;
  final String message;

  static Error buildError(ApiResult result) {
    if (result is ApiError) {
      return Error(
        message: result.error.errors[0],
        statusCode: result.statusCode,
      );
    }
    if (result is NoInternet) {
      return Error(message: Strings.networkErrorMessage);
    }

    final internalError = result as InternalError;
    return Error(
      message: internalError.message,
      statusCode: internalError.statusCode,
    );
  }

  @override
  List<Object> get props => [message, statusCode];
}
