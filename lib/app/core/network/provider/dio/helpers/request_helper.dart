import 'package:dio/dio.dart';
import 'package:empresas_flutter/app/core/network/endpoint/endpoint.dart';
import 'package:empresas_flutter/app/core/network/response/network_response.dart';
import 'package:flutter/cupertino.dart';

abstract class RequestHelper {
  Future<NetworkResponse> makeRequestHelper({
    @required Endpoint endpoint,
    @required Dio httpProvider,
  });
}
