import 'package:dio/dio.dart';
import 'package:empresas_flutter/app/core/network/endpoint/endpoint.dart';
import 'package:empresas_flutter/app/core/network/provider/dio/helpers/request_helper.dart';
import 'package:empresas_flutter/app/core/network/provider/dio/helpers/response_type_dio_helper.dart';
import 'package:empresas_flutter/app/core/network/response/network_response.dart';
import 'package:meta/meta.dart';

class PutHelper implements RequestHelper {
  final _contentTypeHelper = ContentTypeDioResponse();

  @override
  Future<NetworkResponse> makeRequestHelper(
      {@required Endpoint endpoint, @required Dio httpProvider}) async {
    final Response<dynamic> response = await httpProvider.put<dynamic>(
        endpoint.path,
        data: endpoint.parameters,
        options: Options(
            headers: <String, dynamic>{
              ...httpProvider.options.headers,
              ...endpoint.headers ?? <String, dynamic>{},
            },
            responseType:
                _contentTypeHelper.getDioResponseType(endpoint.responseType)),
        queryParameters: endpoint.queryParameters);
    return NetworkResponse(
      data: response.data,
      status: response.statusCode,
    );
  }
}
