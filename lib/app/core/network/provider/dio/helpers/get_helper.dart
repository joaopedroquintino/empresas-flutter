import 'package:dio/dio.dart';
import 'package:empresas_flutter/app/core/network/endpoint/endpoint.dart';
import 'package:empresas_flutter/app/core/network/provider/dio/helpers/request_helper.dart';
import 'package:empresas_flutter/app/core/network/provider/dio/helpers/response_type_dio_helper.dart';
import 'package:empresas_flutter/app/core/network/response/network_response.dart';
import 'package:meta/meta.dart';

class GetHelper implements RequestHelper {
  final _contentTypeHelper = ContentTypeDioResponse();

  @override
  Future<NetworkResponse> makeRequestHelper(
      {@required Endpoint endpoint, @required Dio httpProvider}) async {
    // Map<String, String> queryParameters = endpoint.queryParameters
    //     .map((key, dynamic value) => MapEntry<String, String>(key, '$value'));
    final Response<dynamic> response = await httpProvider.get<dynamic>(
      endpoint.path,
      queryParameters: endpoint.parseQueryParameters(),
      options: Options(
        headers: <String, dynamic>{
          ...httpProvider.options.headers,
          ...endpoint.headers ?? <String, dynamic>{},
        },
        responseType:
            _contentTypeHelper.getDioResponseType(endpoint.responseType),
      ),
    );
    return NetworkResponse(
      data: response.data,
      status: response.statusCode,
    );
  }

  Map<String, String> mapToQueryString(Endpoint endpoint) {
    final Map<String, String> queryParameters =
        endpoint.queryParameters?.map((key, dynamic value) {
      if (value is List) {
        return MapEntry<String, String>(key, value.join(','));
      } else {
        return MapEntry<String, String>(key, '$value');
      }
    });

    return queryParameters;
  }
}
