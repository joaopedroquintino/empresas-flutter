import 'package:dio/dio.dart' as dioResponseType show ResponseType;
import 'package:empresas_flutter/app/core/network/endpoint/endpoint.dart';

class ContentTypeDioResponse {
  final Map<ResponseType, dioResponseType.ResponseType> _map = {
    ResponseType.json: dioResponseType.ResponseType.json,
    ResponseType.plain: dioResponseType.ResponseType.plain,
    ResponseType.bytes: dioResponseType.ResponseType.bytes,
    ResponseType.stream: dioResponseType.ResponseType.stream
  };

  dioResponseType.ResponseType getDioResponseType(ResponseType responseType) {
    return _map[responseType];
  }
}
