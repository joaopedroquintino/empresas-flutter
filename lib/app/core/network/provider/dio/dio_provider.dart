import 'package:dio/dio.dart';
import 'package:empresas_flutter/app/core/network/creators/dio_creator.dart';
import 'package:empresas_flutter/app/core/network/endpoint/endpoint.dart';
import 'package:empresas_flutter/app/core/network/provider/dio/helpers/delete_helper.dart';
import 'package:empresas_flutter/app/core/network/provider/dio/helpers/get_helper.dart';
import 'package:empresas_flutter/app/core/network/provider/dio/helpers/patch_helper.dart';
import 'package:empresas_flutter/app/core/network/provider/dio/helpers/post_helper.dart';
import 'package:empresas_flutter/app/core/network/provider/dio/helpers/put_helper.dart';
import 'package:empresas_flutter/app/core/network/response/network_response.dart';
import 'package:meta/meta.dart';

import '../network_provider.dart';
import 'helpers/request_helper.dart';

class DioProvider implements NetworkProvider {
  Dio _provider;

  Future<NetworkResponse> _safeRequest(
      {@required RequestHelper requestHelper,
      @required Endpoint endpoint}) async {
    assert(requestHelper != null);
    assert(endpoint != null);

    _provider ??= await DioCreator.create(
      timeout: endpoint.timeout,
    );

    try {
      return await requestHelper.makeRequestHelper(
        endpoint: endpoint,
        httpProvider: _provider,
      );
    } on DioError catch (e) {
      if (e.response != null) {
        return NetworkResponse(
          data: e.response.data,
          status: e.response.statusCode,
          typeError: e.type,
        );
      } else {
        return NetworkResponse(
          message: e.message,
          typeError: e.type,
          status: 520,
          data: null,
        );
      }
    }
  }

  @override
  Future<NetworkResponse> get({@required Endpoint endpoint}) async {
    final GetHelper requestHelper = GetHelper();
    return _safeRequest(requestHelper: requestHelper, endpoint: endpoint);
  }

  @override
  Future<NetworkResponse> post({@required Endpoint endpoint}) async {
    return _safeRequest(requestHelper: PostHelper(), endpoint: endpoint);
  }

  @override
  Future<NetworkResponse> put({@required Endpoint endpoint}) {
    final PutHelper requestHelper = PutHelper();
    return _safeRequest(requestHelper: requestHelper, endpoint: endpoint);
  }

  @override
  Future<NetworkResponse> delete({@required Endpoint endpoint}) {
    final DeleteHelper requestHelper = DeleteHelper();
    return _safeRequest(requestHelper: requestHelper, endpoint: endpoint);
  }

  @override
  Future<NetworkResponse> patch({@required Endpoint endpoint}) {
    return _safeRequest(requestHelper: PatchHelper(), endpoint: endpoint);
  }
}
