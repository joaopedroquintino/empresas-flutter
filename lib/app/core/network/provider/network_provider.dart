import 'package:empresas_flutter/app/core/network/endpoint/endpoint.dart';
import 'package:empresas_flutter/app/core/network/response/network_response.dart';
import 'package:meta/meta.dart';

abstract class NetworkProvider {
  Future<NetworkResponse> get({@required Endpoint endpoint});
  Future<NetworkResponse> post({@required Endpoint endpoint});
  Future<NetworkResponse> put({@required Endpoint endpoint});
  Future<NetworkResponse> delete({@required Endpoint endpoint});
  Future<NetworkResponse> patch({@required Endpoint endpoint});
}
