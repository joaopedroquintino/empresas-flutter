import 'package:empresas_flutter/app/core/network/methods/http_method.dart';
import 'package:meta/meta.dart';

class Endpoint {
  Endpoint(
      {@required this.path,
      @required this.method,
      this.responseType = ResponseType.json,
      this.parameters,
      this.queryParameters,
      this.headers,
      this.timeout});

  final String path;
  final HttpMethod method;
  final ResponseType responseType;
  final Map<String, dynamic> headers;
  final Map<String, dynamic> parameters;
  final Map<String, dynamic> queryParameters;
  final int timeout;

  Map<String, String> parseQueryParameters() {
    final Map<String, String> _queryParameters =
        queryParameters?.map((key, dynamic value) {
      if (value is List) {
        return MapEntry<String, String>(key, value.join(','));
      } else {
        return MapEntry<String, String>(key, '$value');
      }
    });

    return _queryParameters;
  }
}

enum ResponseType { json, plain, bytes, stream }
