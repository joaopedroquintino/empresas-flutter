import 'http_method.dart';
import '../response/network_response.dart';
import '../endpoint/endpoint.dart';
import '../provider/network_provider.dart';

class Patch extends HttpMethod {
  @override
  Future<NetworkResponse> request({NetworkProvider http, Endpoint endpoint}) {
    return http.patch(endpoint: endpoint);
  }
}
