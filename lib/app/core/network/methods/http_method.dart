import 'package:empresas_flutter/app/core/network/endpoint/endpoint.dart';
import 'package:empresas_flutter/app/core/network/provider/network_provider.dart';
import 'package:empresas_flutter/app/core/network/response/network_response.dart';
import 'package:meta/meta.dart';

abstract class HttpMethod {
  Future<NetworkResponse> request(
      {@required NetworkProvider http, @required Endpoint endpoint});
}
