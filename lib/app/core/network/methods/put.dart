import 'package:meta/meta.dart';

import 'http_method.dart';
import '../response/network_response.dart';
import '../endpoint/endpoint.dart';
import '../provider/network_provider.dart';

class Put implements HttpMethod {
  @override
  Future<NetworkResponse> request(
      {@required NetworkProvider http, @required Endpoint endpoint}) {
    return http.put(endpoint: endpoint);
  }
}
