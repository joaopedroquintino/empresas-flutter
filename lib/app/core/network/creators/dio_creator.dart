import 'package:dio/dio.dart';
import 'package:empresas_flutter/app/core/network/http/http_config.dart';
import 'package:empresas_flutter/app/core/network/interceptors/logging_interceptor.dart';
import 'package:empresas_flutter/app/core/network/interceptors/request_headers_interceptor.dart';
import 'package:empresas_flutter/config/ambiente.dart';
import 'package:empresas_flutter/injection_container.dart';
import 'package:empresas_flutter/app/core/custom_headers/domain/get_custom_headers_usecase.dart';

class DioCreator {
  static Future<Dio> create({int timeout}) async {
    final Dio dioCreator = Dio()
      ..options.baseUrl = Ambiente.instance.baseUrl
      ..options.connectTimeout =
          timeout ?? HttpConfig.timeoutConfig.connectionTimeout
      ..options.receiveTimeout = HttpConfig.timeoutConfig.receiveTimeout;

    dioCreator.interceptors.add(
      RequestHeadersInterceptor(
          getCustomHeadersUsecase: sl<GetCustomHeadersUsecase>()),
    );

    dioCreator.interceptors.add(LoggingInteceptor());

    return dioCreator;
  }
}
