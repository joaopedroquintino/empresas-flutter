import 'package:empresas_flutter/app/core/network/endpoint/endpoint.dart';
import 'package:empresas_flutter/app/core/network/provider/dio/dio_provider.dart';
import 'package:empresas_flutter/app/core/network/provider/network_provider.dart';
import 'package:empresas_flutter/app/core/network/response/api_result.dart';
import 'package:empresas_flutter/app/core/network/response/network_response.dart';
import 'package:empresas_flutter/app/core/network/response/states/apiError/api_error.dart';
import 'package:empresas_flutter/app/core/network/response/states/apiError/mapped_api_error.dart';
import 'package:empresas_flutter/app/core/network/response/states/internal_error.dart';
import 'package:empresas_flutter/app/core/network/response/states/success.dart';
import 'package:meta/meta.dart';

class ApiManager {
  static final NetworkProvider _networkProvider = DioProvider();

  static InternalError _makeInternalError() {
    return InternalError(
      message:
          'Desculpe, tivemos um problema. Por favor, tente novamente mais tarde.',
      statusCode: 520,
    );
  }

  static Future<ApiResult> request({@required Endpoint endpoint}) async {
    try {
      final NetworkResponse response = await endpoint.method
          .request(http: _networkProvider, endpoint: endpoint);

      if (response.status >= 200 && response.status < 400) {
        return Future<Success>.value(
          Success(
            data: response.data,
            statusCode: response.status,
            headers: response.headers,
          ),
        );
      }

      final MappedApiError mappedError = MappedApiError.fromJson(response.data);
      return Future<ApiError>.value(ApiError(
        error: mappedError,
        statusCode: response.status,
      ));
    } catch (_) {
      return _makeInternalError();
    }
  }
}
