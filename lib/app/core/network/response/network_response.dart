import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

class NetworkResponse {
  NetworkResponse({
    @required this.data,
    @required this.status,
    this.headers,
    this.typeError,
    this.message,
  });

  final dynamic data;
  final int status;
  final String message;
  final dynamic headers;
  final DioErrorType typeError;
}
