import 'package:json_annotation/json_annotation.dart';

part 'mapped_api_error.g.dart';

@JsonSerializable()
class MappedApiError {
  MappedApiError({
    this.success,
    this.errors,
  });

  factory MappedApiError.fromJson(Map<String, dynamic> json) =>
      _$MappedApiErrorFromJson(json);

  Map<String, dynamic> toJson() => _$MappedApiErrorToJson(this);

  @JsonKey(disallowNullValue: true, includeIfNull: false, required: true)
  final bool success;

  @JsonKey(disallowNullValue: true, includeIfNull: false, required: false)
  final List<String> errors;
}
