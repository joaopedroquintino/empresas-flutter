import 'package:empresas_flutter/app/core/network/response/api_result.dart';
import 'package:empresas_flutter/app/core/network/response/states/apiError/mapped_api_error.dart';
import 'package:meta/meta.dart';

class ApiError implements ApiResult {
  ApiError({
    @required this.error,
    @required this.statusCode,
  });

  final MappedApiError error;
  final int statusCode;
}
