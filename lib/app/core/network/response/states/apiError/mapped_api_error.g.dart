// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mapped_api_error.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MappedApiError _$MappedApiErrorFromJson(Map<String, dynamic> json) {
  $checkKeys(json,
      requiredKeys: const ['success'],
      disallowNullValues: const ['success', 'errors']);
  return MappedApiError(
    success: json['success'] as bool,
    errors: (json['errors'] as List)?.map((e) => e as String)?.toList(),
  );
}

Map<String, dynamic> _$MappedApiErrorToJson(MappedApiError instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('success', instance.success);
  writeNotNull('errors', instance.errors);
  return val;
}
