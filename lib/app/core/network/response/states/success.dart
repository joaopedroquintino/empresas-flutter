import 'package:empresas_flutter/app/core/network/response/api_result.dart';
import 'package:meta/meta.dart';

class Success implements ApiResult {
  Success({
    @required this.data,
    @required this.statusCode,
    this.headers,
  });

  final int statusCode;
  final dynamic data;
  final dynamic headers;
}
