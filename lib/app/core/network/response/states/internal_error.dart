import 'package:dio/dio.dart';
import 'package:empresas_flutter/app/core/network/response/api_result.dart';
import 'package:meta/meta.dart';

class InternalError implements ApiResult {
  InternalError(
      {@required this.statusCode, @required this.message, this.typeError});

  final int statusCode;
  final String message;
  final DioErrorType typeError;
}
