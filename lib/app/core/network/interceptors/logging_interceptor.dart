import 'dart:convert';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

class LoggingInteceptor implements Interceptor {
  String _requestUrl;
  String _metodo;

  String _formatarJSON(dynamic data) {
    final encoder = JsonEncoder.withIndent('  ');
    return encoder.convert(data);
  }

  @override
  Future<DioError> onError(DioError err) async {
    debugPrint('<-- ERROR $_requestUrl');
    debugPrint(_isTipoRespostaEmBytes(err.response)
        ? _formatarBytes(err?.response?.data)
        : _formatarJSON(err?.response?.data));
    debugPrint(err?.error);
    debugPrint('<-- END $_metodo');

    return err;
  }

  @override
  Future<RequestOptions> onRequest(RequestOptions options) async {
    _metodo = options.method.toUpperCase();
    final queryParameters = _formatQueryParameters(options.queryParameters);
    _requestUrl = '${options.baseUrl}${options.path}$queryParameters';
    debugPrint('--> $_metodo $_requestUrl');
    debugPrint('Headers: ${options.headers?.toString()}');
    debugPrint(_formatarJSON(options.data));

    return options;
  }

  @override
  Future<Response> onResponse(Response response) async {
    debugPrint(
        '<-- ${response.statusCode} ${response.statusMessage} $_requestUrl');
    debugPrint('Headers: ${response.request.headers?.toString()}');

    debugPrint(
      _isTipoRespostaEmBytes(response)
          ? _formatarBytes(response.data)
          : _formatarJSON(response.data),
    );

    return response;
  }

  String _formatarBytes(dynamic data) {
    final bytes = Uint8List.fromList(data);
    return '--> ${bytes.sublist(0, 5)}...Um monte de bytes...';
  }

  bool _isTipoRespostaEmBytes(Response response) {
    return response.request.responseType == ResponseType.bytes;
  }

  String _formatQueryParameters(Map<String, dynamic> parameters) {
    String queryParameters = '?';
    if (parameters != null) {
      parameters.forEach(
          (String k, dynamic v) => queryParameters = '$queryParameters$k=$v&');
    }
    return queryParameters.substring(0, queryParameters.length - 1);
  }
}
