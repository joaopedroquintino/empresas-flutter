import 'package:dio/dio.dart';
import 'package:empresas_flutter/app/core/custom_headers/domain/get_custom_headers_usecase.dart';
import 'package:meta/meta.dart';

class RequestHeadersInterceptor extends Interceptor {
  RequestHeadersInterceptor({
    @required GetCustomHeadersUsecase getCustomHeadersUsecase,
  }) : _getCustomHeaders = getCustomHeadersUsecase;

  final GetCustomHeadersUsecase _getCustomHeaders;

  @override
  Future onRequest(RequestOptions options) async {
    await _addCustomHeaders(options);
  }

  Future<void> _addCustomHeaders(RequestOptions options) async {
    if (!_hasCustomHeaders(options)) {
      final customHeaders = await _getCustomHeaders();
      if (customHeaders == null) {
        return;
      }
      options.headers.addAll(customHeaders?.toJson());
    }
  }

  bool _hasCustomHeaders(RequestOptions options) {
    return (options?.headers?.containsKey('client') ?? false) &&
        (options?.headers?.containsKey('uid') ?? false) &&
        (options?.headers?.containsKey('access-token') ?? false);
  }
}
