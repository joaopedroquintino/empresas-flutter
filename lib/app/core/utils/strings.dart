abstract class Strings {
  static String networkErrorMessage =
      'Não conseguimos encontrar sinal de internet. Verifique sua conexão e tente novamente';

  static String defaultErrorMessage =
      'Desculpe, tivemos um imprevisto. Por favor, tente novamente mais tarde.';

  static String textSignInButton = 'Entrar';

  static String sigInHeading = 'Seja bem vindo ao empresas!';
}
