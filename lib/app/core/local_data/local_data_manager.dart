import 'dart:convert';

import 'package:empresas_flutter/app/core/local_data/i_local_data_manager.dart';
import 'package:empresas_flutter/app/features/login/data/models/custom_headers_model.dart';
import 'package:empresas_flutter/app/features/login/domain/entities/custom_headers_entity.dart';
import 'package:shared_preferences/shared_preferences.dart';

const _keyCustomHeaders = 'CUSTOM_HEADERS';

class LocalDataManager implements ILocalDataManager {
  static SharedPreferences _storage;

  Future<void> _getStorage() async {
    _storage ??= await SharedPreferences.getInstance();
  }

  @override
  Future<CustomHeadersEntity> getCustomHeaders() async {
    await _getStorage();
    final jsonEncoded = _storage.getString(_keyCustomHeaders);
    if (jsonEncoded == null) {
      return null;
    }
    final _json = json.decode(jsonEncoded) as Map<String, dynamic>;
    return CustomHeadersModel.fromJson(_json);
  }

  @override
  Future<bool> saveCustomHeaders(CustomHeadersEntity customHeaders) async {
    await _getStorage();
    final _json = customHeaders.toJson();

    return _storage.setString(_keyCustomHeaders, json.encode(_json));
  }
}
