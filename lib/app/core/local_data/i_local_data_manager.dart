import 'package:empresas_flutter/app/features/login/domain/entities/custom_headers_entity.dart';

abstract class ILocalDataManager {
  Future<CustomHeadersEntity> getCustomHeaders();
  Future<bool> saveCustomHeaders(CustomHeadersEntity customHeaders);
}
