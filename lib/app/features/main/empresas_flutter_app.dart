import 'package:empresas_flutter/app/core/app_navigator/app_navigator.dart';
import 'package:empresas_flutter/app/features/main/main_navigator/i_main_navigator.dart';
import 'package:empresas_flutter/app/features/main/main_navigator/main_navigator_impl.dart';
import 'package:empresas_flutter/app/features/main/main_navigator/main_routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class EmpresasFlutter extends StatefulWidget {
  @override
  _EmpresasFlutterState createState() => _EmpresasFlutterState();
}

class _EmpresasFlutterState extends State<EmpresasFlutter> {
  final _mainNavigator = MainNavigatorImp();

  @override
  void initState() {
    super.initState();
    AppNavigatorProvider.instance.add<IMainNavigator>(_mainNavigator);
  }

  @override
  void dispose() {
    AppNavigatorProvider.instance.remove<IMainNavigator>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: _mainNavigator.navigationKey,
      debugShowCheckedModeBanner: false,
      title: 'Ioasys Empresas',
      initialRoute: MainRoutes.login,
      onGenerateRoute: MainRoutes.onGenerateRoute,
      locale: Locale('pt', 'BR'),
      supportedLocales: const [Locale('pt', 'BR')],
      localizationsDelegates: [
        GlobalCupertinoLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
    );
  }
}
