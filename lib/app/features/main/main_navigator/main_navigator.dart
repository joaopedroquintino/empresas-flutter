import 'package:empresas_flutter/app/core/app_navigator/app_navigator.dart';
import 'package:empresas_flutter/app/features/main/main_navigator/i_main_navigator.dart';
import 'package:empresas_flutter/app/features/main/main_navigator/main_navigator_impl.dart';
import 'package:empresas_flutter/app/features/main/main_navigator/main_routes.dart';
import 'package:flutter/material.dart';

class MainNavigator extends StatefulWidget {
  @override
  _MainNavigatorState createState() => _MainNavigatorState();
}

class _MainNavigatorState extends State<MainNavigator> {
  final _mainNavigator = MainNavigatorImp();

  @override
  void initState() {
    super.initState();
    AppNavigatorProvider.instance.add<IMainNavigator>(_mainNavigator);
  }

  @override
  void dispose() {
    AppNavigatorProvider.instance.remove<IMainNavigator>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: _mainNavigator.navigationKey,
      onGenerateRoute: MainRoutes.onGenerateRoute,
    );
  }
}
