import 'package:empresas_flutter/app/core/app_navigator/app_navigator.dart';
import 'package:empresas_flutter/app/features/enterprises/domain/entities/enterprise_entity.dart';

abstract class IMainNavigator extends AppNavigator {
  Future<void> goToHome();
  Future<void> goToEnterpriseDetails(EnterpriseEntity enterprise);
  void voltar();
}
