import 'package:empresas_flutter/app/features/enterprises/domain/entities/enterprise_entity.dart';
import 'package:empresas_flutter/app/features/main/main_navigator/i_main_navigator.dart';
import 'package:empresas_flutter/app/features/main/main_navigator/main_routes.dart';
import 'package:flutter/material.dart';

class MainNavigatorImp implements IMainNavigator {
  final _navigationKey = GlobalKey<NavigatorState>();

  @override
  GlobalKey<NavigatorState> get navigationKey => _navigationKey;

  @override
  Future<void> goToHome() {
    return _navigationKey.currentState.pushReplacementNamed(MainRoutes.home);
  }

  @override
  Future<void> goToEnterpriseDetails(EnterpriseEntity enterprise) {
    return _navigationKey.currentState.pushNamed(
      MainRoutes.enterpriseDetails,
      arguments: enterprise,
    );
  }

  @override
  void voltar() {
    _navigationKey.currentState.pop();
  }
}
