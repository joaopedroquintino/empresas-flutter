import 'package:empresas_flutter/app/features/enterprises/domain/entities/enterprise_entity.dart';
import 'package:empresas_flutter/app/features/enterprises/presentation/cubit/enterprises_cubit.dart';
import 'package:empresas_flutter/app/features/enterprises/presentation/pages/enterprise_details_page.dart';
import 'package:empresas_flutter/app/features/enterprises/presentation/pages/enterprises_page.dart';
import 'package:empresas_flutter/app/features/login/presentation/cubit/login_cubit.dart';
import 'package:empresas_flutter/app/features/login/presentation/pages/login_page.dart';
import 'package:empresas_flutter/injection_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class MainRoutes {
  static const login = '/';
  static const home = 'home/';
  static const enterpriseDetails = 'enterprise-details/';

  static Route onGenerateRoute(RouteSettings settings) {
    WidgetBuilder builder;

    switch (settings.name) {
      case MainRoutes.login:
        builder = (context) {
          return BlocProvider(
            create: (context) => sl<LoginCubit>(),
            child: LoginPage(),
          );
        };
        break;
      case MainRoutes.home:
        builder = (context) {
          return BlocProvider(
            create: (context) => sl<EnterprisesCubit>(),
            child: EnterprisesPage(),
          );
        };
        break;
      case MainRoutes.enterpriseDetails:
        final enterprise = settings.arguments as EnterpriseEntity;
        builder = (context) => EnterpriseDetailsPage(enterprise: enterprise);

        break;
    }
    if (builder != null) {
      return MaterialPageRoute(
        builder: builder,
        settings: settings,
      );
    }
    return null;
  }
}
