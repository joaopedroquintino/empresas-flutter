import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/app/core/api/error.dart';
import 'package:empresas_flutter/app/features/enterprises/domain/entities/enterprise_entity.dart';

abstract class IEnterprisesRepository {
  Future<Either<Error, List<EnterpriseEntity>>> getEnterprises({
    String searchText,
  });
  Future<Either<Error, EnterpriseEntity>> getEnterprise(int id);
}
