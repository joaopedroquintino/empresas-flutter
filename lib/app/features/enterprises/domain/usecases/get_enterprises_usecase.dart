import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/app/core/api/error.dart';
import 'package:empresas_flutter/app/features/enterprises/domain/entities/enterprise_entity.dart';
import 'package:empresas_flutter/app/features/enterprises/domain/repositories/i_enterprises_repository.dart';
import 'package:meta/meta.dart';

class GetEnterprisesUseCase {
  GetEnterprisesUseCase({
    @required IEnterprisesRepository enterprisesRepository,
  }) : _repository = enterprisesRepository;

  final IEnterprisesRepository _repository;

  Future<Either<Error, List<EnterpriseEntity>>> call({
    String searchText,
  }) async {
    return _repository.getEnterprises(searchText: searchText);
  }
}
