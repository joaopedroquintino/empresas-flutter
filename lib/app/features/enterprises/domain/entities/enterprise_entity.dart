import 'package:equatable/equatable.dart';

abstract class EnterpriseEntity extends Equatable {
  final int id;
  final String name;
  final String description;
  final double sharePrice;
  final String enterpriseType;
  final String photoUrl;

  EnterpriseEntity({
    this.id,
    this.name,
    this.description,
    this.sharePrice,
    this.enterpriseType,
    this.photoUrl,
  });

  @override
  List<Object> get props => [
        id,
        name,
        description,
        sharePrice,
        enterpriseType,
        this.photoUrl,
      ];
}
