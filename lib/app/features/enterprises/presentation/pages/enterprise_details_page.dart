import 'package:empresas_flutter/app/core/app_navigator/app_navigator.dart';
import 'package:empresas_flutter/app/core/ui/custom_colors.dart';
import 'package:empresas_flutter/app/core/ui/styles.dart';
import 'package:empresas_flutter/app/features/enterprises/domain/entities/enterprise_entity.dart';
import 'package:empresas_flutter/app/features/enterprises/presentation/widgets/card_enterprise_widget.dart';
import 'package:empresas_flutter/app/features/main/main_navigator/i_main_navigator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EnterpriseDetailsPage extends StatefulWidget {
  const EnterpriseDetailsPage({
    @required this.enterprise,
    Key key,
  }) : super(key: key);

  final EnterpriseEntity enterprise;

  @override
  _EnterpriseDetailsPageState createState() => _EnterpriseDetailsPageState();
}

class _EnterpriseDetailsPageState extends State<EnterpriseDetailsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Padding(
            padding: EdgeInsets.only(bottom: 16),
            child: Text(
              widget.enterprise.name,
              style: Styles.headingText,
            ),
          ),
          centerTitle: true,
          leading: Container(
            height: 40,
            width: 40,
            margin: EdgeInsets.only(left: 16, bottom: 16),
            child: CupertinoButton(
              padding: EdgeInsets.zero,
              color: CustomColors.backgroundGrey,
              child: Icon(
                CupertinoIcons.arrow_left,
                color: Colors.red,
              ),
              onPressed:
                  AppNavigatorProvider.instance.get<IMainNavigator>().voltar,
            ),
          ),
        ),
        body: Column(
          children: [
            Hero(
              tag: widget.enterprise.name,
              child: CardEnterpriseWidget(
                enterprise: widget.enterprise,
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    widget.enterprise.description,
                    style: Styles.bodyText,
                  ),
                ),
              ),
            )
          ],
        ));
  }
}
