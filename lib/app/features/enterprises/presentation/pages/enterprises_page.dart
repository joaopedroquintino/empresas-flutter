import 'dart:math' as math;

import 'package:empresas_flutter/app/core/app_navigator/app_navigator.dart';
import 'package:empresas_flutter/app/core/ui/app_images.dart';
import 'package:empresas_flutter/app/core/ui/custom_colors.dart';
import 'package:empresas_flutter/app/core/ui/custom_gradients.dart';
import 'package:empresas_flutter/app/core/ui/styles.dart';
import 'package:empresas_flutter/app/core/ui/widgets/custom_circular_progress.dart';
import 'package:empresas_flutter/app/features/enterprises/presentation/cubit/enterprises_cubit.dart';
import 'package:empresas_flutter/app/features/enterprises/presentation/widgets/card_enterprise_widget.dart';
import 'package:empresas_flutter/app/features/login/presentation/widgets/text_field_widget.dart';
import 'package:empresas_flutter/app/features/main/main_navigator/i_main_navigator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EnterprisesPage extends StatefulWidget {
  @override
  _EnterprisesPageState createState() => _EnterprisesPageState();
}

class _EnterprisesPageState extends State<EnterprisesPage> {
  TextEditingController _searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
    BlocProvider.of<EnterprisesCubit>(context).getEnterprises();

    _searchController.addListener(_searchListener);
  }

  void _searchListener() {
    BlocProvider.of<EnterprisesCubit>(context)
        .searchEnterprises(_searchController.text);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EnterprisesCubit, EnterprisesState>(
      builder: (context, state) {
        return Scaffold(
          body: GestureDetector(
            onTap: () {
              FocusManager.instance.primaryFocus.unfocus();
            },
            behavior: HitTestBehavior.translucent,
            child: CustomScrollView(slivers: [
              SliverAppBar(
                pinned: true,
                collapsedHeight: 84,
                expandedHeight: 184,
                backgroundColor: Colors.transparent,
                elevation: 0,
                flexibleSpace: Container(
                  alignment: Alignment(0, 1),
                  child: Stack(
                    children: [
                      Container(
                        color: CustomColors.color2GradientHome,
                        margin: EdgeInsets.only(bottom: 20),
                      ),
                      FlexibleSpaceBar(
                        background: Stack(
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                gradient: CustomGradients.homeGradient,
                              ),
                              margin: EdgeInsets.only(bottom: 20),
                            ),
                            ..._getImages(),
                          ],
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: TextFieldWidget(
                            controller: _searchController,
                            leading: Icon(
                              CupertinoIcons.search,
                              color: Colors.grey[700],
                            ),
                            placeholder: 'Empresa',
                            style: Styles.textFieldStyle.copyWith(fontSize: 18),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              _buildBody(state),
            ]),
          ),
        );
      },
    );
  }

  List<Widget> _getImages() {
    return [
      Positioned(
        bottom: 20,
        left: 0,
        child: Transform.rotate(
          angle: math.pi / 4,
          child: Image.asset(
            AppImages.ioasysLogo,
            scale: .5,
            color: Colors.white24,
          ),
        ),
      ),
      Positioned(
        top: -20,
        left: 50,
        child: Transform.rotate(
          angle: 5 * math.pi / 4,
          child: Image.asset(
            AppImages.ioasysLogo,
            scale: .2,
            color: Colors.white24,
          ),
        ),
      ),
      Positioned(
        top: 20,
        right: -30,
        child: Transform.rotate(
          angle: math.pi,
          child: Image.asset(
            AppImages.ioasysLogo,
            scale: .4,
            color: Colors.white24,
          ),
        ),
      ),
      Positioned(
        bottom: 30,
        right: 50,
        child: Transform.rotate(
          angle: 3.5 * math.pi / 4,
          child: Image.asset(
            AppImages.ioasysLogo,
            scale: .4,
            color: Colors.white24,
          ),
        ),
      ),
    ];
  }

  Widget _buildBody(EnterprisesState state) {
    if (state is EnterprisesError) {
      return SliverFillRemaining(
        child: Center(
          child: Text('Erro'),
        ),
      );
    }

    if (state is EnterprisesLoaded) {
      return SliverPadding(
        padding: EdgeInsets.only(top: 20),
        sliver: SliverAnimatedList(
          initialItemCount: state.enterprises.length + 1,
          itemBuilder: (context, index, animaion) {
            if (index == 0) {
              if (state.hasSearch ?? false) {
                return Padding(
                  padding: const EdgeInsets.fromLTRB(16, 8, 0, 8),
                  child: Text(
                    '${state.enterprises.length} resultados encontrados',
                    style: Styles.textFieldStyle,
                  ),
                );
              } else {
                return Container();
              }
            }

            final enterprise = state.enterprises[index - 1];

            return Padding(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 16),
              child: Hero(
                tag: enterprise.name,
                child: CardEnterpriseWidget(
                  enterprise: enterprise,
                  onPressed: () {
                    FocusManager.instance.primaryFocus.unfocus();
                    AppNavigatorProvider.instance
                        .get<IMainNavigator>()
                        .goToEnterpriseDetails(enterprise);
                  },
                ),
              ),
            );
          },
        ),
      );
    }
    return SliverFillRemaining(
      child: Center(
        child: CustomCircularProgress(),
      ),
    );
  }
}
