part of 'enterprises_cubit.dart';

abstract class EnterprisesState extends Equatable {
  const EnterprisesState();

  @override
  List<Object> get props => [];
}

class EnterprisesInitial extends EnterprisesState {}

class EnterprisesLoading extends EnterprisesState {}

class EnterprisesLoaded extends EnterprisesState {
  EnterprisesLoaded(this.enterprises, {this.hasSearch = false});
  final List<EnterpriseEntity> enterprises;
  final bool hasSearch;

  @override
  List<Object> get props => [enterprises, hasSearch];
}

class EnterprisesError extends EnterprisesState {
  EnterprisesError(this.message);
  final String message;

  @override
  List<Object> get props => [message];
}
