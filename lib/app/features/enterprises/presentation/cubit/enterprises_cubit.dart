import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:empresas_flutter/app/features/enterprises/domain/entities/enterprise_entity.dart';
import 'package:empresas_flutter/app/features/enterprises/domain/usecases/get_enterprises_usecase.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'enterprises_state.dart';

class EnterprisesCubit extends Cubit<EnterprisesState> {
  EnterprisesCubit({@required GetEnterprisesUseCase getEnterprisesUseCase})
      : _getEnterprises = getEnterprisesUseCase,
        super(EnterprisesInitial());

  final GetEnterprisesUseCase _getEnterprises;
  Timer _debounce;

  Future<void> getEnterprises() async {
    emit(EnterprisesLoading());

    final result = await _getEnterprises();

    result.fold(
      (error) => emit(EnterprisesError(error.message)),
      (enterprises) => emit(EnterprisesLoaded(enterprises)),
    );
  }

  Future<void> searchEnterprises(String text) async {
    _initDebounce(
      callback: () => _searchEnterprises(text),
    );
  }

  Future<void> _searchEnterprises(String text) async {
    if (text.isEmpty) {
      if (state is EnterprisesLoaded) {
        if (!(state as EnterprisesLoaded).hasSearch) {
          return;
        }
      }
      return getEnterprises();
    }

    emit(EnterprisesLoading());

    final result = await _getEnterprises(searchText: text);

    result.fold(
      (error) => emit(EnterprisesError(error.message)),
      (enterprises) => emit(EnterprisesLoaded(enterprises, hasSearch: true)),
    );
  }

  void _initDebounce({Function callback}) {
    if (_debounce?.isActive ?? false) {
      _debounce?.cancel();
    }

    _debounce = Timer(Duration(milliseconds: 700), () async {
      callback();
    });
  }
}
