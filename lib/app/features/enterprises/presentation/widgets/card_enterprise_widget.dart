import 'package:empresas_flutter/app/core/ui/styles.dart';
import 'package:empresas_flutter/app/features/enterprises/domain/entities/enterprise_entity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CardEnterpriseWidget extends StatelessWidget {
  const CardEnterpriseWidget({
    Key key,
    this.onPressed,
    this.enterprise,
  }) : super(key: key);
  final VoidCallback onPressed;
  final EnterpriseEntity enterprise;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: CupertinoButton(
          onPressed: onPressed,
          padding: EdgeInsets.zero,
          child: Stack(children: [
            Container(
              color: _getColorById(enterprise.id).withOpacity(.8),
            ),
            Container(
              alignment: Alignment.center,
              child: Text(
                enterprise.name.toUpperCase(),
                style: Styles.headingEnterpriseStyle,
              ),
            ),
          ]),
        ),
      ),
    );
  }
}

Color _getColorById(int id) {
  switch (id % 6) {
    case 0:
      return Colors.blue[300];
      break;
    case 1:
      return Colors.green[400];
      break;
    case 2:
      return Colors.pink[200];
      break;
    case 3:
      return Colors.amber[400];
      break;
    case 4:
      return Colors.brown[300];
      break;
    case 5:
      return Colors.deepOrange[500];
      break;
    default:
      return Colors.black;
  }
}
