import 'package:empresas_flutter/app/features/enterprises/domain/entities/enterprise_entity.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

class EnterpriseModel extends EnterpriseEntity {
  EnterpriseModel({
    @required int id,
    @required String name,
    String description,
    double sharePrice,
    String enterpriseType,
    String photoUrl,
  }) : super(
          id: id,
          name: name,
          description: description,
          sharePrice: sharePrice,
          enterpriseType: enterpriseType,
          photoUrl: photoUrl,
        );

  factory EnterpriseModel.fromJson(Map<String, dynamic> json) {
    $checkKeys(
      json,
      requiredKeys: ['id', 'enterprise_name', 'description', 'enterprise_type'],
      disallowNullValues: [
        'id',
        'enterprise_name',
        'description',
        'enterprise_type'
      ],
    );

    return EnterpriseModel(
        id: (json['id'] as num).toInt(),
        name: json['enterprise_name'] as String,
        description: json['description'] as String,
        enterpriseType: json['enterprise_type']['enterprise_type_name'],
        photoUrl: json['photo'],
        sharePrice: json['share_price']);
  }
}
