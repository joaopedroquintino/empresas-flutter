import 'package:empresas_flutter/app/core/network/endpoint/endpoint.dart';
import 'package:empresas_flutter/app/core/network/manager/api_manager.dart';
import 'package:empresas_flutter/app/core/network/methods/get.dart';
import 'package:empresas_flutter/app/core/network/response/api_result.dart';

class EnterprisesDataSource {
  String _urlEnterprises(int id) => 'enterprises/${id ?? ''}';

  Future<ApiResult> getEnterprises({
    Map<String, dynamic> queryParameters,
    int id,
  }) async {
    final _endpoint = Endpoint(
      path: _urlEnterprises(id),
      method: Get(),
      queryParameters: queryParameters,
    );

    return ApiManager.request(endpoint: _endpoint);
  }
}
