import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/app/core/api/error.dart';
import 'package:empresas_flutter/app/core/network/response/api_result.dart';
import 'package:empresas_flutter/app/core/network/response/states/success.dart';
import 'package:empresas_flutter/app/core/utils/strings.dart';
import 'package:empresas_flutter/app/features/enterprises/data/datasources/enterprises_datasource.dart';
import 'package:empresas_flutter/app/features/enterprises/data/models/enterprise_model.dart';
import 'package:empresas_flutter/app/features/enterprises/domain/entities/enterprise_entity.dart';
import 'package:empresas_flutter/app/features/enterprises/domain/repositories/i_enterprises_repository.dart';
import 'package:meta/meta.dart';

class EnterprisesRepositoryImpl implements IEnterprisesRepository {
  EnterprisesRepositoryImpl(
      {@required EnterprisesDataSource enterprisesDataSource})
      : _dataSource = enterprisesDataSource;

  final EnterprisesDataSource _dataSource;

  @override
  Future<Either<Error, EnterpriseEntity>> getEnterprise(int id) async {
    final result = await _dataSource.getEnterprises(id: id);

    if (result is Success) {
      try {
        final enterprise = EnterpriseModel.fromJson(result.data['enterprise']);
        return Right(enterprise);
      } catch (_) {
        return Left(Error(message: Strings.defaultErrorMessage));
      }
    } else {
      return Left(Error.buildError(result));
    }
  }

  @override
  Future<Either<Error, List<EnterpriseEntity>>> getEnterprises({
    String searchText,
  }) async {
    Map<String, dynamic> query;
    if (searchText != null) {
      query = <String, dynamic>{
        'name': searchText,
      };
    }

    final result = await _dataSource.getEnterprises(queryParameters: query);
    return _decodeEnterprisesList(result);
  }

  Either<Error, List<EnterpriseEntity>> _decodeEnterprisesList(
      ApiResult result) {
    if (result is Success) {
      try {
        final enterprises = (result.data['enterprises'] as List)
            .map(
              (json) => EnterpriseModel.fromJson(json),
            )
            .toList();
        return Right(enterprises);
      } catch (_) {
        return Left(Error(message: Strings.defaultErrorMessage));
      }
    } else {
      return Left(Error.buildError(result));
    }
  }
}
