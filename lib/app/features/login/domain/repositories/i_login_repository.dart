import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/app/core/api/error.dart';
import 'package:empresas_flutter/app/features/login/domain/entities/custom_headers_entity.dart';
import 'package:meta/meta.dart';

abstract class ILoginRepository {
  Future<Either<Error, CustomHeadersEntity>> login({
    @required String user,
    @required String password,
  });
}
