import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/app/core/api/error.dart';
import 'package:empresas_flutter/app/core/local_data/i_local_data_manager.dart';
import 'package:empresas_flutter/app/core/utils/strings.dart';
import 'package:empresas_flutter/app/features/login/domain/entities/custom_headers_entity.dart';
import 'package:meta/meta.dart';

class SaveCustomHeadersUseCase {
  SaveCustomHeadersUseCase({
    @required ILocalDataManager localDataManager,
  }) : _localStorage = localDataManager;

  final ILocalDataManager _localStorage;

  Future<Either<Error, Unit>> call(CustomHeadersEntity customHeaders) async {
    final savedHeaders = await _localStorage.saveCustomHeaders(customHeaders);

    if (savedHeaders) {
      return Right(unit);
    }

    return Left(Error(message: Strings.defaultErrorMessage));
  }
}
