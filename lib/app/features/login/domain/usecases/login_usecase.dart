import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/app/core/api/error.dart';
import 'package:empresas_flutter/app/features/login/domain/entities/custom_headers_entity.dart';
import 'package:empresas_flutter/app/features/login/domain/repositories/i_login_repository.dart';
import 'package:meta/meta.dart';

class LoginUseCase {
  LoginUseCase({
    @required ILoginRepository loginRepository,
  }) : _repository = loginRepository;

  final ILoginRepository _repository;

  Future<Either<Error, CustomHeadersEntity>> call({
    @required String email,
    @required String password,
  }) async {
    return _repository.login(user: email, password: password);
  }
}
