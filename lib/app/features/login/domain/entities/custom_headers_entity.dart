import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class CustomHeadersEntity extends Equatable {
  CustomHeadersEntity({
    @required this.accessToken,
    @required this.uID,
    @required this.client,
  });

  final String accessToken;
  final String uID;
  final String client;

  Map<String, dynamic> toJson();

  @override
  List<Object> get props => [accessToken, uID, client];
}
