import 'package:empresas_flutter/app/core/network/endpoint/endpoint.dart';
import 'package:empresas_flutter/app/core/network/manager/api_manager.dart';
import 'package:empresas_flutter/app/core/network/methods/post.dart';
import 'package:empresas_flutter/app/core/network/response/api_result.dart';
import 'package:meta/meta.dart';

class LoginDataSource {
  Future<ApiResult> login({
    @required Map<String, dynamic> parameters,
  }) {
    const _urlLogin = 'users/auth/sign_in';

    final _endpoint = Endpoint(
      path: _urlLogin,
      method: Post(),
      parameters: parameters,
    );

    return ApiManager.request(endpoint: _endpoint);
  }
}
