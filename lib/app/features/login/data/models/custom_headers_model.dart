import 'package:empresas_flutter/app/features/login/domain/entities/custom_headers_entity.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

class CustomHeadersModel extends CustomHeadersEntity {
  CustomHeadersModel({
    @required String accessToken,
    @required String uID,
    @required String client,
  }) : super(
          accessToken: accessToken,
          uID: uID,
          client: client,
        );

  factory CustomHeadersModel.fromJson(Map<String, dynamic> json) {
    $checkKeys(
      json,
      requiredKeys: ['access-token', 'uid', 'client'],
      disallowNullValues: ['access-token', 'uid', 'client'],
    );
    return CustomHeadersModel(
      accessToken: json['access-token'][0] as String,
      uID: json['uid'][0] as String,
      client: json['client'][0] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'access-token': [accessToken],
      'uid': [uID],
      'client': [client],
    };
  }
}
