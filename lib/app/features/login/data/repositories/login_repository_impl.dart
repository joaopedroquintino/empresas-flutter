import 'package:empresas_flutter/app/core/network/response/states/success.dart';
import 'package:empresas_flutter/app/core/utils/strings.dart';
import 'package:empresas_flutter/app/features/login/data/datasources/login_datasource.dart';
import 'package:empresas_flutter/app/features/login/data/models/custom_headers_model.dart';
import 'package:empresas_flutter/app/features/login/domain/entities/custom_headers_entity.dart';
import 'package:empresas_flutter/app/core/api/error.dart';
import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/app/features/login/domain/repositories/i_login_repository.dart';
import 'package:meta/meta.dart';

class LoginRepositoryImpl implements ILoginRepository {
  LoginRepositoryImpl({
    @required LoginDataSource loginDataSource,
  }) : _dataSource = loginDataSource;

  final LoginDataSource _dataSource;

  @override
  Future<Either<Error, CustomHeadersEntity>> login({
    String user,
    String password,
  }) async {
    final _parameters = <String, dynamic>{
      'email': user,
      'password': password,
    };

    final result = await _dataSource.login(parameters: _parameters);

    if (result is Success) {
      try {
        final _customHeaders = CustomHeadersModel.fromJson(result.headers);

        return Right(_customHeaders);
      } catch (_) {
        return Left(Error(message: Strings.defaultErrorMessage));
      }
    } else {
      return Left(Error.buildError(result));
    }
  }
}
