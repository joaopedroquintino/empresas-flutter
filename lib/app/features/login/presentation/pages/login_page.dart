import 'dart:ui' as ui;

import 'package:empresas_flutter/app/core/app_navigator/app_navigator.dart';
import 'package:empresas_flutter/app/core/ui/app_images.dart';
import 'package:empresas_flutter/app/core/ui/custom_colors.dart';
import 'package:empresas_flutter/app/core/ui/styles.dart';
import 'package:empresas_flutter/app/core/ui/widgets/loading_blur_screen.dart';
import 'package:empresas_flutter/app/core/utils/strings.dart';
import 'package:empresas_flutter/app/features/login/presentation/cubit/login_cubit.dart';
import 'package:empresas_flutter/app/features/login/presentation/widgets/login_form_widget.dart';
import 'package:empresas_flutter/app/features/main/main_navigator/i_main_navigator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage>
    with SingleTickerProviderStateMixin {
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    bool _hasKeyboard = MediaQuery.of(context).viewInsets.bottom > 0;
    return BlocConsumer<LoginCubit, LoginState>(
      listener: (context, state) {
        if (state is LoginSuccess) {
          AppNavigatorProvider.instance.get<IMainNavigator>().goToHome();
        }
      },
      builder: (context, state) {
        return LoadingBlurScreen(
          enabled: state is LoginLoading,
          child: Scaffold(
            backgroundColor: Colors.white,
            body: GestureDetector(
              onTap: () {
                FocusManager.instance.primaryFocus.unfocus();
              },
              behavior: HitTestBehavior.translucent,
              child: Container(
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Container(
                      child: CustomPaint(
                        size: Size.infinite,
                        painter: AppBarCustomPainter(),
                        child: SafeArea(
                          child: Center(
                            child: Padding(
                              padding:
                                  const EdgeInsets.fromLTRB(16, 32, 16, 42),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Image.asset(AppImages.ioasysLogo),
                                  AnimatedContainer(
                                    height: _hasKeyboard ? 0 : 50,
                                    duration: Duration(milliseconds: 200),
                                    alignment: Alignment.center,
                                    child: Text(
                                      Strings.sigInHeading,
                                      style: Styles.headingSignInStyle,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: SingleChildScrollView(
                        padding: EdgeInsets.only(top: 40),
                        child: Container(
                          alignment: Alignment(0, -.5),
                          padding: EdgeInsets.all(20),
                          child: LoginFormWidget(
                            emailController: _emailController,
                            passwordController: _passwordController,
                            hasError: state is LoginError,
                            errorMessage:
                                state is LoginError ? state.message : null,
                            onSignIn: () async {
                              FocusManager.instance.primaryFocus.unfocus();
                              context.read<LoginCubit>().signIn(
                                    email: _emailController.text,
                                    password: _passwordController.text,
                                  );
                            },
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class AppBarCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint_0 = new Paint()
      ..color = Color.fromARGB(255, 255, 255, 255)
      ..style = PaintingStyle.fill
      ..strokeWidth = 1
      ..shader = ui.Gradient.linear(
        Offset(size.width, 0),
        Offset(0, size.height),
        [
          CustomColors.color1GradientLogin,
          CustomColors.color2GradientLogin,
        ],
      );

    Path path_0 = Path();
    path_0.moveTo(0, size.height * 0.7016317);
    path_0.quadraticBezierTo(size.width * 0.2511364, size.height * 0.8175758,
        size.width * 0.5050505, size.height * 0.8181818);
    path_0.quadraticBezierTo(size.width * 0.7578535, size.height * 0.8149650,
        size.width, size.height * 0.7016317);
    path_0.lineTo(size.width, size.height * 0.0013054);
    path_0.lineTo(0, 0);
    path_0.lineTo(0, size.height * 0.7016317);
    path_0.close();

    canvas.drawPath(path_0, paint_0);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
