import 'package:empresas_flutter/app/core/ui/custom_colors.dart';
import 'package:empresas_flutter/app/core/ui/styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TextFieldWidget extends StatelessWidget {
  const TextFieldWidget({
    @required this.controller,
    Key key,
    this.focusNode,
    this.borderColor,
    this.placeholder,
    this.leading,
    this.obscureText = false,
    this.trailling,
    this.style,
  }) : super(key: key);

  final FocusNode focusNode;
  final Color borderColor;
  final String placeholder;
  final Widget leading;
  final Widget trailling;
  final bool obscureText;
  final TextEditingController controller;
  final TextStyle style;

  @override
  Widget build(BuildContext context) {
    return CupertinoTextField(
      controller: controller,
      focusNode: focusNode,
      placeholder: placeholder,
      padding: EdgeInsets.all(12),
      maxLines: 1,
      style: style ?? Styles.textFieldStyle,
      cursorHeight: 26,
      cursorWidth: 1,
      cursorColor: CustomColors.mainColor,
      decoration: BoxDecoration(
        color: CustomColors.backgroundGrey,
        borderRadius: BorderRadius.circular(4),
        border: borderColor != null ? Border.all(color: borderColor) : null,
      ),
      prefix: leading == null
          ? null
          : Padding(
              padding: EdgeInsets.only(left: 12),
              child: leading,
            ),
      suffix: trailling,
      obscureText: obscureText,
    );
  }
}
