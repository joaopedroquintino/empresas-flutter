import 'package:empresas_flutter/app/core/ui/custom_colors.dart';
import 'package:empresas_flutter/app/core/ui/styles.dart';
import 'package:empresas_flutter/app/core/utils/strings.dart';
import 'package:empresas_flutter/app/features/login/presentation/widgets/text_field_caption_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginFormWidget extends StatelessWidget {
  const LoginFormWidget({
    @required this.emailController,
    @required this.passwordController,
    @required this.onSignIn,
    Key key,
    this.hasError = false,
    this.errorMessage,
  }) : super(key: key);

  final bool hasError;
  final String errorMessage;
  final TextEditingController emailController;
  final TextEditingController passwordController;
  final VoidCallback onSignIn;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisSize: MainAxisSize.min,
      children: [
        TextFieldCaptionWidget(
          controller: emailController,
          caption: 'Email',
          hasError: hasError,
        ),
        SizedBox(height: 16),
        TextFieldCaptionWidget(
          controller: passwordController,
          caption: 'Password',
          hasError: hasError,
          obscureText: true,
        ),
        Text(
          errorMessage ?? '',
          textAlign: TextAlign.end,
          style: Styles.errorStyle,
        ),
        CupertinoButton(
          color: CustomColors.mainColor,
          child: Text(
            Strings.textSignInButton,
            style: Styles.signInButtonStyle,
          ),
          onPressed: onSignIn,
        ),
      ],
    );
  }
}
