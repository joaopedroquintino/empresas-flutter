import 'package:empresas_flutter/app/core/ui/custom_colors.dart';
import 'package:empresas_flutter/app/features/login/presentation/widgets/text_field_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TextFieldCaptionWidget extends StatefulWidget {
  const TextFieldCaptionWidget({
    Key key,
    this.hasError = false,
    this.caption,
    this.obscureText = false,
    @required this.controller,
    this.trailling,
  }) : super(key: key);

  final bool hasError;
  final String caption;
  final bool obscureText;
  final TextEditingController controller;
  final Widget trailling;

  @override
  _TextFieldCaptionWidgetState createState() => _TextFieldCaptionWidgetState();
}

class _TextFieldCaptionWidgetState extends State<TextFieldCaptionWidget> {
  final focusNode = FocusNode();
  Color labelColor = Colors.grey;
  bool _showingText = true;

  @override
  void initState() {
    super.initState();
    _showingText = !widget.obscureText;

    focusNode.addListener(_focusListener);
  }

  @override
  void dispose() {
    focusNode.removeListener(_focusListener);
    focusNode.dispose();
    super.dispose();
  }

  Widget _getEyeWidget(bool showing) {
    return CupertinoButton(
      child: Icon(
        showing ? CupertinoIcons.eye_slash_fill : CupertinoIcons.eye_fill,
        color: Colors.grey[700],
      ),
      onPressed: () {
        setState(() {
          _showingText = !_showingText;
        });
      },
    );
  }

  void _focusListener() {
    if (focusNode.hasFocus) {
      if (labelColor != CustomColors.mainColor) {
        setState(() {
          labelColor = CustomColors.mainColor;
        });
      }
    } else {
      if (labelColor != Colors.grey) {
        setState(() {
          labelColor = Colors.grey;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        if (widget.caption != null)
          Text(
            widget.caption,
            textAlign: TextAlign.left,
            style: TextStyle(
              color: labelColor,
            ),
          ),
        TextFieldWidget(
          controller: widget.controller,
          focusNode: focusNode,
          borderColor: widget.hasError ? Colors.redAccent : null,
          obscureText: !_showingText,
          trailling: widget.obscureText ? _getEyeWidget(_showingText) : null,
        ),
      ],
    );
  }
}
