import 'package:bloc/bloc.dart';
import 'package:empresas_flutter/app/features/login/domain/usecases/login_usecase.dart';
import 'package:empresas_flutter/app/features/login/domain/usecases/save_custom_headers_usecase.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  LoginCubit({
    @required LoginUseCase loginUseCase,
    @required SaveCustomHeadersUseCase saveCustomHeadersUseCase,
  })  : _login = loginUseCase,
        _saveCustomHeaders = saveCustomHeadersUseCase,
        super(LoginInitial());

  final LoginUseCase _login;
  final SaveCustomHeadersUseCase _saveCustomHeaders;

  Future<void> signIn({
    @required String email,
    @required String password,
  }) async {
    emit(LoginLoading());

    final result = await _login(email: email, password: password);

    result.fold(
      (error) {
        emit(LoginError(error.message));
      },
      (customHeaders) async {
        final saveResult = await _saveCustomHeaders(customHeaders);
        saveResult.fold(
          (error) => emit(LoginError(error.message)),
          (_) => emit(LoginSuccess()),
        );
      },
    );
  }
}
