import 'dart:async';

import 'package:empresas_flutter/app/features/main/empresas_flutter_app.dart';
import 'package:empresas_flutter/config/ambiente.dart';
import 'package:empresas_flutter/injection_container.dart' as dependencies;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      systemNavigationBarIconBrightness: Brightness.dark,
      systemNavigationBarColor: Colors.transparent,
      systemNavigationBarDividerColor: Colors.transparent,
    ),
  );
  SystemChrome.setPreferredOrientations(
    [DeviceOrientation.portraitUp],
  );

  Ambiente(baseUrl: 'https://empresas.ioasys.com.br/api/v1/');

  runZonedGuarded(
    () async {
      await dependencies.init();

      runApp(EmpresasFlutter());
    },
    (error, stackTrace) {},
  );
}
//red="0.58465" green="0.00214" blue="0.99691"
