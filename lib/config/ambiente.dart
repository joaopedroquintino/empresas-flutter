import 'package:meta/meta.dart';

class Ambiente {
  factory Ambiente({
    @required String baseUrl,
  }) {
    _instance ??= Ambiente._internal(baseUrl);
    return _instance;
  }

  Ambiente._internal(this.baseUrl);

  static Ambiente get instance {
    return _instance;
  }

  final String baseUrl;

  static Ambiente _instance;
}
