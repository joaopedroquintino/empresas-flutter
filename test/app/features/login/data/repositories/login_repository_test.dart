import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/app/core/api/error.dart';
import 'package:empresas_flutter/app/core/network/response/states/apiError/api_error.dart';
import 'package:empresas_flutter/app/core/network/response/states/apiError/mapped_api_error.dart';
import 'package:empresas_flutter/app/core/network/response/states/success.dart';
import 'package:empresas_flutter/app/features/login/data/datasources/login_datasource.dart';
import 'package:empresas_flutter/app/features/login/data/models/custom_headers_model.dart';
import 'package:empresas_flutter/app/features/login/data/repositories/login_repository_impl.dart';
import 'package:empresas_flutter/app/features/login/domain/repositories/i_login_repository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class _MockLoginDataSource extends Mock implements LoginDataSource {}

void main() {
  _MockLoginDataSource _mockDatasource;
  ILoginRepository _repository;

  setUp(() {
    _mockDatasource = _MockLoginDataSource();
    _repository = LoginRepositoryImpl(loginDataSource: _mockDatasource);
  });

  group('Login repository tests', () {
    final _tMapResponseHeaders = <String, dynamic>{
      'uid': ['bkauydi7yti3hb'],
      'client': ['8uayodbkmnad'],
      'access-token': ['kuhg7y2u3h3h3h'],
    };

    final _tUser = 'test@ioasys.com.br';
    final _tPassword = '1782686';
    final _tParameters = {'email': _tUser, 'password': _tPassword};

    final _tCustomHeadersModel =
        CustomHeadersModel.fromJson(_tMapResponseHeaders);

    final _tErrorMessage = 'Invalid login credentials. Please try again.';
    final _tMappedError = MappedApiError(
      success: false,
      errors: [_tErrorMessage],
    );
    final _tApiError = ApiError(error: _tMappedError, statusCode: 401);

    test('Should sign in succesfully', () async {
      when(_mockDatasource.login(parameters: anyNamed('parameters')))
          .thenAnswer(
        (_) async => Success(
          data: null,
          statusCode: 200,
          headers: _tMapResponseHeaders,
        ),
      );

      final result = await _repository.login(
        user: _tUser,
        password: _tPassword,
      );

      expect(result, Right(_tCustomHeadersModel));
      verify(_mockDatasource.login(parameters: _tParameters)).called(1);
      verifyNoMoreInteractions(_mockDatasource);
    });

    test('Should fail in sign in', () async {
      when(_mockDatasource.login(parameters: anyNamed('parameters')))
          .thenAnswer(
        (_) async => _tApiError,
      );

      final result = await _repository.login(
        user: _tUser,
        password: _tPassword,
      );

      expect(result, Left(Error.buildError(_tApiError)));
      verify(_mockDatasource.login(parameters: _tParameters)).called(1);
      verifyNoMoreInteractions(_mockDatasource);
    });
  });
}
