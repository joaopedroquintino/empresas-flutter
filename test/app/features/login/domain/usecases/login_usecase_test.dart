import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/app/core/api/error.dart';
import 'package:empresas_flutter/app/features/login/data/models/custom_headers_model.dart';
import 'package:empresas_flutter/app/features/login/domain/repositories/i_login_repository.dart';
import 'package:empresas_flutter/app/features/login/domain/usecases/login_usecase.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class _MockLoginRepository extends Mock implements ILoginRepository {}

void main() {
  LoginUseCase _loginUseCase;
  _MockLoginRepository _mockRepository;

  setUp(() {
    _mockRepository = _MockLoginRepository();
    _loginUseCase = LoginUseCase(loginRepository: _mockRepository);
  });

  group('Login usecase tests', () {
    final _tUser = 'test@ioasys.com.br';
    final _tPassword = '1782686';

    final _tCustomHeadersModel = CustomHeadersModel(
      client: 'uhiuhiu',
      uID: 'iugugk',
      accessToken: 'iugkygg3g',
    );

    final _tError = Error(message: 'I don\'t know what happened here');

    test('Should call repository and sign in succesfully', () async {
      when(_mockRepository.login(
              user: anyNamed('user'), password: anyNamed('password')))
          .thenAnswer((_) async => Right(_tCustomHeadersModel));

      final result = await _loginUseCase(email: _tUser, password: _tPassword);

      expect(result, Right(_tCustomHeadersModel));
      verify(_mockRepository.login(user: _tUser, password: _tPassword));
      verifyNoMoreInteractions(_mockRepository);
    });

    test('Should call repository and fail in sign in', () async {
      when(_mockRepository.login(
              user: anyNamed('user'), password: anyNamed('password')))
          .thenAnswer((_) async => Left(_tError));

      final result = await _loginUseCase(email: _tUser, password: _tPassword);

      expect(result, Left(_tError));
      verify(_mockRepository.login(user: _tUser, password: _tPassword));
      verifyNoMoreInteractions(_mockRepository);
    });
  });
}
