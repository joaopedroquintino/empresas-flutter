import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/app/core/api/error.dart';
import 'package:empresas_flutter/app/features/login/data/models/custom_headers_model.dart';
import 'package:empresas_flutter/app/features/login/domain/usecases/login_usecase.dart';
import 'package:empresas_flutter/app/features/login/domain/usecases/save_custom_headers_usecase.dart';
import 'package:empresas_flutter/app/features/login/presentation/cubit/login_cubit.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class _MockLoginUseCase extends Mock implements LoginUseCase {}

class _MockSaveCustomHeadersUseCase extends Mock
    implements SaveCustomHeadersUseCase {}

void main() {
  LoginCubit _cubit;
  _MockLoginUseCase _mockLoginUseCase;
  _MockSaveCustomHeadersUseCase _mockSaveCustomHeadersUseCase;

  setUp(() {
    _mockLoginUseCase = _MockLoginUseCase();
    _mockSaveCustomHeadersUseCase = _MockSaveCustomHeadersUseCase();

    _cubit = LoginCubit(
      loginUseCase: _mockLoginUseCase,
      saveCustomHeadersUseCase: _mockSaveCustomHeadersUseCase,
    );
  });

  tearDown(() {
    _cubit.close();
  });

  group('Login cubit tests', () {
    final _tCustomHeaders = CustomHeadersModel(
      accessToken: 'a234',
      uID: 'a7iyk',
      client: 'kugy876',
    );

    final _tUser = 'test@ioasys.com.br';
    final _tPassword = '1782686';
    final _tError = Error(message: 'Invalid Credentials');

    test('Ensures the Initial State', () async {
      expect(_cubit.state, LoginInitial());
    });

    blocTest<LoginCubit, LoginState>(
      'Should emit states correctly when sign in successfully',
      build: () {
        when(_mockLoginUseCase(
                email: anyNamed('email'), password: anyNamed('password')))
            .thenAnswer((realInvocation) async => Right(_tCustomHeaders));
        when(_mockSaveCustomHeadersUseCase(any))
            .thenAnswer((_) async => Right(unit));

        return _cubit;
      },
      act: (cubit) async {
        await cubit.signIn(email: _tUser, password: _tPassword);
      },
      expect: <LoginState>[
        LoginLoading(),
        LoginSuccess(),
      ],
      verify: (cubit) {
        verify(_mockLoginUseCase(email: _tUser, password: _tPassword))
            .called(1);
        verify(_mockSaveCustomHeadersUseCase(_tCustomHeaders)).called(1);
        verifyNoMoreInteractions(_mockLoginUseCase);
        verifyNoMoreInteractions(_mockSaveCustomHeadersUseCase);
      },
    );

    blocTest<LoginCubit, LoginState>(
      'Should emit states correctly when failing in sign in ',
      build: () {
        when(_mockLoginUseCase(
                email: anyNamed('email'), password: anyNamed('password')))
            .thenAnswer((realInvocation) async => Left(_tError));
        return _cubit;
      },
      act: (cubit) async {
        await cubit.signIn(email: _tUser, password: _tPassword);
      },
      expect: <LoginState>[
        LoginLoading(),
        LoginError(_tError.message),
      ],
      verify: (cubit) {
        verify(_mockLoginUseCase(email: _tUser, password: _tPassword))
            .called(1);
        verifyNoMoreInteractions(_mockLoginUseCase);
      },
    );

    blocTest<LoginCubit, LoginState>(
      'Should emit states correctly when sign in successfully and fail saving custom headers',
      build: () {
        when(_mockLoginUseCase(
                email: anyNamed('email'), password: anyNamed('password')))
            .thenAnswer((realInvocation) async => Right(_tCustomHeaders));
        when(_mockSaveCustomHeadersUseCase(any))
            .thenAnswer((_) async => Left(_tError));

        return _cubit;
      },
      act: (cubit) async {
        await cubit.signIn(email: _tUser, password: _tPassword);
      },
      expect: <LoginState>[
        LoginLoading(),
        LoginError(_tError.message),
      ],
      verify: (cubit) {
        verify(_mockLoginUseCase(email: _tUser, password: _tPassword))
            .called(1);
        verify(_mockSaveCustomHeadersUseCase(_tCustomHeaders)).called(1);
        verifyNoMoreInteractions(_mockLoginUseCase);
        verifyNoMoreInteractions(_mockSaveCustomHeadersUseCase);
      },
    );
  });
}
